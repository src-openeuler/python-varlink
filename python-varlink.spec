Name:           python-varlink
Version:        31.0.0
Release:        1
Summary:        Python implementation of the Varlink protocol
License:        ASL 2.0
URL:            https://github.com/varlink/%{name}
Source0:        https://github.com/varlink/%{name}/archive/%{version}/%{name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python3-devel python3-rpm-macros
BuildRequires:  python3-setuptools python3-setuptools_scm

%description
An implementation of the Varlink protocol with python module. For
server implementations use the varlink.Server class. For client
implementations use the varlink.Client class.

%package -n python3-varlink
Summary:        Python implementation of the Varlink protocol

%{?python_provide:%python_provide python3-varlink}
Obsoletes:      python-varlink

%description -n python3-varlink
An implementation of the Varlink protocol with python module. For
server implementations use the varlink.Server class. For client
implementations use the varlink.Client class.

%prep
%autosetup -n varlink-%{version} -p1

%build
%py3_build

%check
CFLAGS="%{optflags}" %{__python3} %{py_setup} %{?py_setup_args} check

%install
%py3_install

%files -n python3-varlink
%doc README.md LICENSE.txt
%{python3_sitelib}/*

%changelog
* Thu Jun 23 2022 SimpleUpdate Robot <tc@openeuler.org> - 31.0.0-1
- Upgrade to version 31.0.0

* Thu Feb 20 2020 Jiangping Hu <hujp1985@foxmail.com> - 27.1.1-2
- Package init
